pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ChoteToken is ERC20, ERC20Detailed, ERC20Mintable {
    uint public initialSupply = 1000000;

    constructor(
        string memory _name, 
        string memory _symbol, 
        uint8 _decimals
    )
    ERC20Detailed(_name, _symbol, _decimals) 
    public {

    }

}