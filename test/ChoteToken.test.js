const BigNumber = web3.BigNumber;

const ChoteToken = artifacts.require("ChoteToken");

require("chai").use(require("chai-bignumber")(BigNumber)).should();

contract("ChoteToken", (accounts) => {
  const _name = "Chote Token";
  const _symbol = "CHT";
  const _decimals = 18;

  beforeEach(async function () {
    this.token = await ChoteToken.new(_name, _symbol, _decimals);
  });

  describe("token attributes", function () {
    it("has the correct name", async function () {
      const name = await this.token.name();
      name.should.equal(_name);
    });

    it("has the correct symbol", async function () {
      const symbol = await this.token.symbol();
      symbol.should.equal(_symbol);
    });

    // it("has the correct decimals", async function () {
    //   const decimals = await this.token.decimals();
    //   decimals.should.be.bignumber.equal(_decimals);
    // });

    it("check balance", async function () {
      const balance = await this.token.totalSupply();
      const eth = web3.utils.fromWei(balance, "ether");

      console.log(eth + ' ETH');
    });
  });
});
