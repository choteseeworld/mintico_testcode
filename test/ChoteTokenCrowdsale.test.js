const BigNumber = web3.BigNumber;

require("chai").use(require("chai-bignumber")(BigNumber)).should();

const ChoteToken = artifacts.require("ChoteToken");
const ChoteTokenCrowdsale = artifacts.require("ChoteTokenCrowdsale");

function ether(n) {
  return new web3.utils.toBN(web3.utils.toWei(n, "ether"));
}

contract("ChoteTokenCrowdsale", function ([_, wallet, investor1, investor2]) {
  beforeEach(async function () {
    this.name = "Chote Token";
    this.symbol = "CHT";
    this.decimals = 18;

    this.token = await ChoteToken.new(this.name, this.symbol, this.decimals);

    // Crowdsale config
    this.rate = new web3.utils.BN(20000);
    this.wallet = wallet;

    this.crowdsale = await ChoteTokenCrowdsale.new(
      this.rate,
      this.wallet,
      this.token.address
    );
    // Binance Smart Chain Faucet

    await this.token.addMinter(this.crowdsale.address, { from: _ });
    await this.token.renounceMinter({ from: _ });
  });

  describe("crowdsale", function () {
    it("tracks the rate", async function () {
      const rate = await this.crowdsale.rate();
      // console.log("rate " + rate);
      //   rate.should.be.bignumber.equal(this.rate);
    });

    it("tracks the wallet", async function () {
      const wallet = await this.crowdsale.wallet();
      wallet.should.equal(this.wallet);
    });

    it("tracks the token", async function () {
      const token = await this.crowdsale.token();
      token.should.equal(this.token.address);
    });
  });

  describe("minted crowdsale", function () {
    it("mints tokens after purchase", async function () {
      const originalTotalSupply = await this.token.totalSupply();

      await this.crowdsale.sendTransaction({
        value: ether("0.015"),
        from: wallet,
      });
      const newTotalSupply = await this.token.totalSupply();
      // console.log("originalTotalSupply: " + originalTotalSupply);
      // console.log("newTotalSupply: " + newTotalSupply / 10 ** 18);
      assert.isTrue(newTotalSupply > originalTotalSupply);
    });
  });

});
