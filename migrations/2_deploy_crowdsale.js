const ChoteTokenCrowdsale = artifacts.require("ChoteTokenCrowdsale");
const ChoteToken = artifacts.require("ChoteToken");

const Web3 = require("web3");

function ether(n) {
  return new Web3.utils.BN(Web3.utils.toWei(n, "ether"));
}

module.exports = async function (deployer, network, accounts) {
  const _name = "Chote Token";
  const _symbol = "CHT";
  const _decimals = 18;
  const rate = new Web3.utils.BN(20000); // At 20,000 Token/ETH
  const wallet = "0x04aa9D8feD0286a75c237ff459019e6c03Ebca0c"; // accounts[0]

  const wallet01 = "0x32495A62af7c0da6E9815680631f60Ac32d45a4a";

  try {
    await deployer.deploy(ChoteToken, _name, _symbol, _decimals, {
      from: wallet,
    });
    const token = await ChoteToken.deployed();

    await deployer.deploy(
      ChoteTokenCrowdsale,
      rate,
      wallet,
      token.address,
      wallet01
    );
    const crowdsale = await ChoteTokenCrowdsale.deployed();
    // console.log(crowdsale);

    // await token.addMinter(crowdsale.address);
    // await token.renounceMinter();

    await token.mint(wallet01, "1000000000000000000000");
    // await token.transferOwnership(crowdsale.address)
    // await web3.eth.sendTransaction({
    //   to: crowdsale.address,
    //   from: wallet01,
    //   value: ether("0.01"),
    // });

    // Balance to ETH
    // const tokenBalance = await token.totalSupply();
    // console.log(tokenBalance);
    // const eth = web3.utils.fromWei(tokenBalance, "ether");
    // console.log("totalSupply: " + tokenBalance);

    console.log("Token address: ", token.address);
    console.log("Crowdsale address: ", crowdsale.address);

    return true;
  } catch (e) {
    console.log("Error sending transaction: " + e.message);
  }
};
